let webpack = require('webpack')
let ngAnnotateWebpackPlugin = require('ng-annotate-webpack-plugin');
let htmlWebpackPlugin = require('html-webpack-plugin');
let extractTextPlugin = require('extract-text-webpack-plugin');
let extractPlugin = new extractTextPlugin({
    filename : 'main.css'
})
module.exports = {
    entry : './src/index.js',
    output : {
        path : __dirname + '/dist',
        filename : 'bundle.js',
        // publicPath : '/dist'
    },
    module : {
        rules : [
            {
                test : /\.js$/ , loader : 'babel-loader', exclude : /node_module/
            },
            {
                test : /\.scss$/ ,
                use : extractPlugin.extract({
                    use : [
                        'css-loader',
                        'sass-loader'
                    ]
                })
            },
            {
                test : /\.html$/,
                loader : 'html-loader'
            },
            {
                test : /\.(jpg|png)$/,
                use : [
                    {
                        loader : 'file-loader',
                        options : {
                            name : '[name].[ext]',
                            outputPath : 'img/',
                            publicPath : 'img/'
                        }
                    }
                ]
            }
        ]
    },
    plugins : [
        new ngAnnotateWebpackPlugin({
            add : true
        }),
        extractPlugin,
        new htmlWebpackPlugin({
            template : 'src/index.html'
        })
        
    ],
    devServer : {
        // publicPath : '/dist',
        contentBase : __dirname + '/',
        compress : false
    }
}