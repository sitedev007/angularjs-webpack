module.exports = function(...allowed){
    const isAllowed = role => (allowed.indexOf(role) > -1 || allowed.indexOf('all')  > -1);
    return function(req,res,next) {
        if(req.user && isAllowed(req.user.role))
            next();
        else
            res.status(401).json({ success : false , error :  'Permission Denied'});
    }
}