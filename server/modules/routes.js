let express = require('express');
const path = require('path');
module.exports = {
    init : function(app){
        this.bindMiddlewares(app);
        this.bindRoutes(app);
        this.bindStatics(app);
        this.bindAuthentication(app);
        this.bindPermission(app);
    },
    bindRoutes : function(app){
        const userApi = require('./user/user.route');
        app.use('/api/user',userApi);
    },
    bindStatics : function(app){
        app.use(express.static(path.join(appPath,'client/dist')));
        app.get('/*',function(req,res){
            res.sendFile(appPath + '/client/dist/index.html');
        })
    },
    bindMiddlewares : function(app){

    },
    bindAuthentication : function(app){

    },
    bindPermission : function(app){

    }
}