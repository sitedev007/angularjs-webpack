module.exports = function(req,res,next){
    let token = req.get('authorization');
    if(token && utils.auth.isUserLoggedIn(token)){
        let verify = utils.decodeToken(token);
        if(verify.success){
            req.user = verify.payload;
            next();
        }else
            res.json({success : false, error : verify.error});   
    }else
        res.json({success : false, error : 'Authentication Failed!'});   
}