const { ObjectId, MongoClient } = require('mongodb');

let dbUrl = '', dbName = '';
let _db;
module.exports = {
    connect : function(callback){
        MongoClient.connect(dbUrl,function(err,client){
            if(err) return;

            _db = client.db(dbName);
            if(callback) _db;
        })
    },
    toObjectId : function(id){
        return ObjectId(id)
    },
    getCollection : function(collectionName){
        return _db.collection(collectionName);
    }
}