
let config = {};
module.exports = {
    load : function(environment){
        config.database = require(`./database/` + environment);
        global.config = config;
    }
}