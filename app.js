require('dotenv').config({path: __dirname + '/.env'});

let express = require('express');
let app = express();
let PORT = process.env.PORT || 3000;
global.appPath = __dirname;
// load config
require('./server/config').load(process.env.NODE_ENV);

app.listen(PORT,function(){
    console.log('server started  on port : ', PORT);
    require('./server/modules/routes').init(app);
})